{
  description = "volare";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.volare = let pkgs = nixpkgs.legacyPackages.x86_64-linux; in pkgs.stdenv.mkDerivation {
      pname = "volare";
      version = "1.9.0.0-snapshot";
      src = ./.;
      strictDeps = true;
      depsBuildBuild = [ pkgs.pkg-config ];
      nativeBuildInputs = [
        pkgs.meson pkgs.ninja pkgs.pkg-config pkgs.wayland-scanner pkgs.scdoc
      ];
      buildInputs = [
        pkgs.wayland pkgs.libxkbcommon pkgs.pcre2 pkgs.json_c pkgs.libevdev
        pkgs.pango pkgs.cairo pkgs.libinput pkgs.libcap pkgs.pam pkgs.gdk-pixbuf pkgs.librsvg
        pkgs.wayland-protocols pkgs.libdrm
        #(pkgs.enableDebugging (pkgs.wlroots_0_17.override { enableXWayland = true; }))
        (pkgs.wlroots_0_17.override { enableXWayland = true; })
        pkgs.dbus
        # for xcb/xcb-icccm.h
        pkgs.xorg.xcbutilwm.dev
        # for glesv2
        pkgs.libGL
      ];
      mesonFlags = [ "-Dsd-bus-provider=libsystemd" "--buildtype=debugoptimized" ];
      doCheck = true;
      dontStrip = true;
      checkPhase = ''
        runHook preCheck
        meson test --verbose
        runHook postCheck
      '';

      # The nix build sandbox doesn't have /usr/bin/env, since it has no standardized implementation,
      # so we have to spell out the path to the python interpreter:
      preInstall = ''
        sed -i "s|#\!/usr/bin/env python3|#\!${pkgs.python3}/bin/python3|" ../meson_volare_rename.py
      '';
      meta = with pkgs.lib; {
        description = "Volare is a tabbed, tiling Wayland compositor.";
        longDescription = ''
          Compared to other tiling compositors, volare is more static: new windows
          will show up as tabs in the current frame instead of rearranging the screen
          layout.

          Volare is a branch of Sway, so typically tools designed for Sway are
          intended to work with volare as well.
        '';
        homepage = "https://codeberg.org/raboof/volare";
        license = licenses.mit;
        maintainers = with maintainers; [ raboof ];
        platforms = platforms.linux;
        mainProgram = "volare";
      };
    };

    packages.x86_64-linux.default = self.packages.x86_64-linux.volare;

  };
}
