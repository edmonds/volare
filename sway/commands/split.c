#include <string.h>
#include <strings.h>
#include "sway/commands.h"
#include "sway/tree/arrange.h"
#include "sway/tree/container.h"
#include "sway/tree/view.h"
#include "sway/tree/workspace.h"
#include "sway/input/input-manager.h"
#include "sway/input/seat.h"
#include "log.h"

static struct cmd_results *do_split(int layout) {
	struct sway_container *con = config->handler_context.container;
	struct sway_workspace *ws = config->handler_context.workspace;
	if (con) {
		if (container_is_scratchpad_hidden_or_child(con) &&
				con->pending.fullscreen_mode != FULLSCREEN_GLOBAL) {
			return cmd_results_new(CMD_FAILURE,
					"Cannot split a hidden scratchpad container");
		}
		container_split(con, layout);
	} else {
		if (!workspace_split(ws, layout)) {
			workspace_add_empty_tabs(ws);
		}
	}

	if (root->fullscreen_global) {
		arrange_root();
	} else {
		arrange_workspace(ws);
	}

	seat_consider_warp_to_focus(config->handler_context.seat);

	return cmd_results_new(CMD_SUCCESS, NULL);
}

static struct sway_container *find_unsplit_destination(struct sway_container *current, struct sway_workspace* ws, struct sway_container *ancestor) {
	list_t *siblings;
	if (ancestor) {
		siblings = ancestor->pending.children;
	} else {
		siblings = ws->tiling;
	}
	if (siblings->length == 1) {
		return NULL;
	}
	for (int i = 0; i < siblings->length; i++) {
		if (siblings->items[i] == current) {
			if (i == 0) {
				return siblings->items[1];
			} else {
				return siblings->items[i-1];
			}
		}
	}
	return NULL;
 }

static struct cmd_results *do_unsplit(void) {
	struct sway_container *con = config->handler_context.container;
	struct sway_workspace *ws = config->handler_context.workspace;

	// Look for a suitable ancestor of the container to move within
	struct sway_container *current = con;
	struct sway_container *ancestor = NULL;
	while (current && !ancestor) {
		struct sway_container *parent = current->pending.parent;
		if (parent) {
			if (parent->pending.layout != L_TABBED && parent->pending.children->length > 1) {
				ancestor = parent;
			} else {
				current = current->pending.parent;
			}
		} else {
			current = current->pending.parent;
		}
	}
	if (!ancestor) {
		current = con;
		while (current->pending.parent) {
			current = current->pending.parent;
		}
	}

	struct sway_container *destination = find_unsplit_destination(current, ws, ancestor);
	if (!destination) {
		sway_log(SWAY_DEBUG, "No destination container found for unsplit");
		return cmd_results_new(CMD_SUCCESS, NULL);
	}
	if (destination->is_placeholder) {
		sway_log(SWAY_DEBUG, "Unsplit destination is placeholder");
		return cmd_results_new(CMD_SUCCESS, NULL);
	}
	if (!destination->pending.children) {
		sway_log(SWAY_DEBUG, "Unsplit destination has no children list");
		return cmd_results_new(CMD_SUCCESS, NULL);
	}
	struct sway_container *parent = con->pending.parent;
	while (parent->pending.children->length) {
		struct sway_container *child = parent->pending.children->items[0];
		if (child->is_placeholder) {
			container_begin_destroy(child);
		} else {
			container_add_child(destination, child);
			child->pending.width = child->pending.height = 0;
			child->width_fraction = child->height_fraction = 0;
		}
	}

	struct sway_container *gp = parent->pending.parent;
	container_begin_destroy(parent);
	if (gp != NULL) {
		container_flatten(gp);
	}

	workspace_squash(destination->pending.workspace);

	if (root->fullscreen_global) {
		arrange_root();
	} else {
		arrange_workspace(ws);
	}

	return cmd_results_new(CMD_SUCCESS, NULL);
}

struct cmd_results *cmd_split(int argc, char **argv) {
	struct cmd_results *error = NULL;
	if ((error = checkarg(argc, "split", EXPECTED_EQUAL_TO, 1))) {
		return error;
	}
	if (!root->outputs->length) {
		return cmd_results_new(CMD_INVALID,
				"Can't run this command while there's no outputs connected.");
	}
	if (strcasecmp(argv[0], "v") == 0 || strcasecmp(argv[0], "vertical") == 0) {
		return do_split(L_VERT);
	} else if (strcasecmp(argv[0], "h") == 0 ||
			strcasecmp(argv[0], "horizontal") == 0) {
		return do_split(L_HORIZ);
	} else if (strcasecmp(argv[0], "t") == 0 ||
			strcasecmp(argv[0], "toggle") == 0) {
		struct sway_container *focused = config->handler_context.container;

		if (focused && container_parent_layout(focused) == L_VERT) {
			return do_split(L_HORIZ);
		} else {
			return do_split(L_VERT);
		}
	} else if (strcasecmp(argv[0], "n") == 0 ||
			strcasecmp(argv[0], "none") == 0) {
		return do_unsplit();
	} else {
		return cmd_results_new(CMD_FAILURE,
			"Invalid split command (expected either horizontal or vertical).");
	}
	return cmd_results_new(CMD_SUCCESS, NULL);
}

struct cmd_results *cmd_splitv(int argc, char **argv) {
	struct cmd_results *error = NULL;
	if ((error = checkarg(argc, "splitv", EXPECTED_EQUAL_TO, 0))) {
		return error;
	}
	return do_split(L_VERT);
}

struct cmd_results *cmd_splith(int argc, char **argv) {
	struct cmd_results *error = NULL;
	if ((error = checkarg(argc, "splith", EXPECTED_EQUAL_TO, 0))) {
		return error;
	}
	return do_split(L_HORIZ);
}

struct cmd_results *cmd_splitt(int argc, char **argv) {
	struct cmd_results *error = NULL;
	if ((error = checkarg(argc, "splitt", EXPECTED_EQUAL_TO, 0))) {
		return error;
	}

	struct sway_container *con = config->handler_context.container;

	if (con && container_parent_layout(con) == L_VERT) {
		return do_split(L_HORIZ);
	} else {
		return do_split(L_VERT);
	}
}
