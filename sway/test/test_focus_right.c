#include <stdio.h>
#include <string.h>
#include "framework.c"

int main() {
	initialize();

	struct sway_workspace *ws = workspace_create(root->outputs->items[0], "WS1");
	ws->layout = L_HORIZ;
	struct sway_container *one = container_create(NULL);
	struct sway_container *other = container_create(NULL);
	workspace_add_tiling(ws, one);
	workspace_add_tiling(ws, other);
	config->handler_context.node = &one->node;

	list_t *results = execute_command("focus right", NULL, one);
	
	int n_results = results->length;
	if (n_results == 0) {
		printf("did not receive any result\n");
		return -1;
	}
	for (int i=0; i<n_results; i++) {
		struct cmd_results *result = results->items[i];
		if (result->status != CMD_SUCCESS) {
			printf("unsuccessful result: '%s'\n", result->error);
			return -1;
		}
	}
	if (seat_get_focus(&default_seat) == &other->node)
		return 0;
	else {
		printf("not ok other node was focused\n");
		return -1;
	}
}
