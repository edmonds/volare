#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "sway/commands.h"
#include "sway/input/seat.h"
#include "sway/output.h"
#include "sway/tree/container.h"
#include "sway/tree/node.h"
#include "sway/tree/workspace.h"

// memdup and ALLOC_INIT based on
// https://tia.mat.br/posts/2015/05/01/initializing_a_heap_allocated_structure_in_c.html
void *memdup(const void *src, size_t sz) {
        void *mem = malloc(sz);
        return mem ? memcpy(mem, src, sz) : NULL;
}
#define ALLOC_INIT(type, ...)   \
        (type *)memdup((type[]){ __VA_ARGS__ }, sizeof(type))

static struct sway_seat default_seat = {
};
void sway_cursor_constrain(struct sway_cursor *cursor,
    struct wlr_pointer_constraint_v1 *constraint) {};
struct sway_seat *input_manager_get_default_seat(void) {
    return &default_seat;
}
struct sway_seat *input_manager_current_seat(void) {
    return &default_seat;
}
struct wl_client *wl_resource_get_client(struct wl_resource *resource) {
    return NULL;
}
void input_manager_configure_xcursor(void) {}
void ipc_event_workspace(struct sway_workspace *old, struct sway_workspace *new, const char *change) {}
void ipc_event_window(struct sway_container *window, const char *change) {}
void wlr_seat_keyboard_notify_clear_focus(struct wlr_seat *seat) {}
struct wlr_addon *wlr_addon_find(struct wlr_addon_set *set, const void *owner, const struct wlr_addon_interface *i) {
    return NULL;
}

static const struct sway_view_impl empty_view_impl = {
     .get_constraints = NULL,
     .get_string_prop = NULL,
     .configure = NULL,
     .set_activated = NULL,
     .set_tiled = NULL,
     .set_fullscreen = NULL,
     .set_resizing = NULL,
     .wants_floating = NULL,
     .for_each_surface = NULL,
     .for_each_popup_surface = NULL,
     .is_transient_for = NULL,
     .close = NULL,
     .close_popups = NULL,
     .destroy = NULL,
};

struct sway_server server = {0};

void initialize() {
    //load_main_config("../config.in", false, true);
    struct sway_input_manager *input = 
            calloc(1, sizeof(struct sway_input_manager));
    wl_list_init(&input->devices);
    wl_list_init(&input->seats);
    struct sway_server testserver = {
        .dirty_nodes = create_list(),
        .input = input,
    };
    server = testserver;
    struct sway_mode mode = {
        .name = "testmode",
        .keysym_bindings = create_list(),
        .keycode_bindings = create_list(),
    };
    config = ALLOC_INIT(struct sway_config, {
        .active = true,
        .current_mode = &mode,
        .workspace_configs = create_list(),
    });
    root = root_create();
    struct wlr_output* fallback_output = ALLOC_INIT(struct wlr_output, {
        .name = "FALLBACK",
    });
    root->fallback_output = output_create(fallback_output);
    struct wlr_output* output = ALLOC_INIT(struct wlr_output, {
        .name = "OUT1",
    });
     
    output_enable(output_create(output));

    wl_list_init(&default_seat.focus_stack);
    wl_list_init(&default_seat.devices);
    default_seat.deferred_bindings = create_list();
}
